1.- ¿En que año fue creado GIT?
	RESP: 2005
2.- ¿Quien es reconocido como el creador de GIT?
	RESP: Linus Torvalds
3.- ¿Que comando puedo usar para listar la configuracion global de GIT instalado en mi PC?
	RESP: git config --list
4.- ¿Que comando puedo usar para incializar un repositorio local?
	RESP: git init
5.- Porfavor liste en orden las 3 lineas de comando necesario para realizar un commit simple.
	RESP:
		> git init
		> git add .
		> git commit -m "comentario"

6.- ¿Que comando usamos para listar el historial de commits?
	RESP: git log
7.- Por favor escriba el comnado para clonar un repositorio remoto.
	RESP: git clone <URL_REPOSITORIO_REMOTO>
8.- ¿Como se llama el "apuntador" que nos indica donde estamos posicionados en nuestro repositorio?
	RESP: HEAD
9.- Si intentamos borrar una rama local y obtenemos el siguiente mensaje 
			>error: The branch 'mi_rama' is not fully merged.
    Si estamos seguros de querer borrar dicha rama ¿que comando usamos?	
	RESP: git branch -D mi_rama
10.- Indique las lineas de comando para crear una rama y posicionarme en esta.
	RESP: git checkout -b <nombre_rama>
11.- ¿Para que nos sirve el REAMDE.md?
	RESP: muestra información referente al proyecto como: prerequisitos, forma de instalación, pruebas ejecutadas, autores, etc.
12.- ¿Para que usamos el archivo .gitignore?
	RESP: Para excluir archivos o carpetas.
13.- Si nos piden que resolvamos un problema en produccion cuando estamos agregando nueva funcionalidad
     al proyecto, que comando podemos usar para guardar temporalmente nuestros avances?
	RESP: git stash
14.- Indique solo un tipo de flujo de trabajo distribuido
	RESP: Dictador-Tenientes
15.- Indique cuales son las ramas princiaples para gitflow
	RESP: master, develop.
16.- Indique cuales son las ramas de apoyo en gitflow
	RESP: Feature, Release, Hotfix

